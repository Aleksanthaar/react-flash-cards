import React, {Component} from 'react';
import './App.css';

import * as jsonCategories from './cards.json';

import CategoriesList from './CategoriesList/CategoriesList';
import Card from './Card/Card';

export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      categories: this.makeCategories(jsonCategories),
      card: false
    }

    this.state.card = this.pickCard()
  }

  componentDidMount(){
    document.addEventListener("keydown", this.keyPressHandler, false);
  }

  keyPressHandler = event => {
    if (32 === event.keyCode) {
        this.newCardHandler()
        event.preventDefault()

        return;
    }

    if (96 <= event.keyCode && event.keyCode <= 105) {
      const index     = (event.keyCode - 96)
      const itemIndex = this.indexToItemKey(index)

      this.doToggleItem(itemIndex)
      event.preventDefault()

      return;
    }
  }

  makeCategories = function(json) {
    let jsonCategories  = json.default.categories
    let stateCategories = [];

    for (const key of Object.keys(jsonCategories)) {
      stateCategories.push({
        'label': key,
        'id': key,
        'checked': true,
        'cards': jsonCategories[key]
      })
    }

    return stateCategories
  }

  changeAction = (event) => {
    let toggleItem = event.currentTarget.id

    this.doToggleItem(toggleItem)
  }

  indexToItemKey = (index) => this.state.categories[index].label

  doToggleItem = (index) => {
    let newItems = this.state.categories.map(item => {
      if (item.id === index) {
        item.checked = !item.checked
      }

      return item;
    });

    this.setState({ categories: newItems})
  }

  newCardHandler = () => {
    let newCard = this.pickCard()

    this.setState({ card: newCard })
  }

  pickCard = () => {
    let { categories } = this.state

    let pickableCategories = categories.filter(cat => cat.checked)

    // Pick from none = pick from all
    if (0 === pickableCategories.length) {
      pickableCategories = categories
    }

    let selectedCategory = Math.floor((Math.random() * pickableCategories.length));
    let selectedCard     = Math.floor((Math.random() * pickableCategories[selectedCategory].cards.length));
    let card             = pickableCategories[selectedCategory].cards[selectedCard]

    card.category = pickableCategories[selectedCategory].label

    return card
  }

  render() {
    const { categories }                     = this.state
    let { tail, head, info, link, category } = this.state.card

    return (
      <main className="app">
        <header>
          <h1>Flashcards with React!</h1>
          <small>(and terrible design skills)</small>
        </header>
        <section className="container">
          <CategoriesList categories={categories} changeAction={this.changeAction} />
          <section>
            <Card category={category} tail={tail} info={info} head={head} link={link} isFlipped={false}>{info}</Card>
            <button onClick={this.newCardHandler}>Draw new card</button>
          </section>
        </section>
        <footer><strong>Pro tip</strong>: Space Bar = new card ; Enter = flip card</footer>
      </main>
    );
  }
}
