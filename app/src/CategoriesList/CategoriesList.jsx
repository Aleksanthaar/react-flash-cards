import React from 'react';
import './CategoriesList.css'

export default function CategoriesList({ categories, changeAction }) {
    return (
        <nav>
            <h3>Categories:</h3>
            <ul className="categories">
                {categories.map((item, i) => (
                    <li key={i} id={item.id} className={item.checked ? 'enabled' : ''} onClick={changeAction}>
                        <label>
                            <input type="checkbox" id={item.id} checked={item.checked ? 'checked' : ''} onChange={changeAction} />
                            {item.label} ({i})</label>
                    </li>
                ))}
            </ul>
        </nav>
    );
}