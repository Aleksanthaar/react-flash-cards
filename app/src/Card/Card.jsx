import React, { Component } from 'react'
import './Card.css';

export default class Card extends Component {
    constructor(props) {
        super(props)

        this.state = {
            'category': props.category,
            'head': props.head,
            'tail': props.tail,
            'info': props.info,
            'link': props.link,
            'isFlipped': props.isFlipped
        }
    }

    componentDidMount(){
        document.addEventListener("keydown", this.keyPressHandler, false);
    }

    componentWillReceiveProps(nextProps) {
        this.setState(nextProps);
    }

    keyPressHandler = event => {
        if (13 === event.keyCode) { // Enter
          event.preventDefault()
          this.doFlip()
        }
    }    

    flipHandler = (e) => {
        if (e.target.nodeName === 'A') {
            return
        }

        this.doFlip()
    }

    doFlip = () => {
        this.setState({
            isFlipped: !this.state.isFlipped
        })
    }

    render() {
        const { category, head, tail, info, link, isFlipped } = this.state

        return(
            <article className="card" onClick={this.flipHandler}>
                <aside>{ category }</aside>
                {isFlipped ? (
                    <p>
                        <strong className="answer">{tail}</strong>
                        <span dangerouslySetInnerHTML={{__html: info}} ></span>
                        <br /><br />
                        <a href={link} title="Voir la source">Source</a>
                    </p>
                ) : <h2>{head}</h2> }
            </article>
        )
    }
}
