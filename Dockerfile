FROM node:12.5-stretch-slim

WORKDIR /home/node/app

EXPOSE 3000

CMD [ "./start.sh" ]