# How to use

```bash
    $ make image
    $ make container
    # enjoy
```

To stop and remove the container, run `make halt`.