.PHONY: image container halt bash

image:
	@echo "Creating image ..." && docker build -treactfhlashcards .

container:
	@docker run -d -v "${PWD}/app:/home/node/app" --name=reactfhlashcards reactfhlashcards ; \
	echo "\nContainer running, wait a few seconds and visit http://`docker inspect reactfhlashcards | jq .[0].NetworkSettings.Networks.bridge.IPAddress | cut -d \\" -f2`:3000" ; \
	echo "If you are running this for the first time, give npm a few extra seconds to install dependencies."

halt:
	@docker container stop reactfhlashcards && docker container rm reactfhlashcards

# Enters container for debugging purposes.
bash:
	@docker run --rm -it -v "${PWD}/app:/home/node/app" --name=reactfhlashcards reactfhlashcards bash